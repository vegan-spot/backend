from app import create_app, db
from app.models import Product, Cart, Order, Component, ProductType

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'Product': Product,
        'Cart': Cart,
        'Order': Order,
        'Component': Component,
        'ProductType': ProductType,
    }