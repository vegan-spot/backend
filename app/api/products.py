from flask import jsonify, request

from app.models import Product, ProductType

from app.api import bp


@bp.route('/products/<int:product_id>', methods=['GET'])
def get_product(product_id):
    return jsonify(Product.query.get_or_404(product_id).to_dict())


@bp.route('/products', methods=['POST'])
def create_product():
    pass


@bp.route('/products', methods=['DELETE'])
def delete_product(product_id):
    pass


@bp.route('/products', methods=['PUT'])
def update_product(product_id):
    pass


@bp.route('/products', methods=['GET'])
def get_products():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Product.to_collection_dict(Product.query, page, per_page, 'api.get_products')
    return jsonify(data)


@bp.route('/products/components/<int:product_id>', methods=['GET'])
def get_components_of_product(product_id):
    product = Product.query.get_or_404(product_id)
    return jsonify(product.get_components())
