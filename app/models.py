from hashlib import md5
from datetime import datetime, timedelta
import enum
from json import JSONEncoder

from flask import current_app, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin

from app import db, login


class PaginatedAPIMixin:
    @staticmethod
    def to_collection_dict(query, page, per_page, endpoint, **kwargs):
        resources = query.paginate(page, per_page, False)
        data = {
            'items': [item.to_dict() for item in resources.items],
            '_meta': {
                'page': page,
                'per_page': per_page,
                'total_pages': resources.pages,
                'total_items': resources.total
            },
            '_links': {
                'self': url_for(endpoint, page=page, per_page=per_page, **kwargs),
                'next': url_for(endpoint, page=page + 1, per_page=per_page, **kwargs) if resources.has_next else None,
                'prev': url_for(endpoint, page=page - 1, per_page=per_page, **kwargs) if resources.has_prev else None
            }
        }
        return data


cart_product = db.Table('cart_product',
                        db.Column('product_id', db.Integer, db.ForeignKey('product.id')),
                        db.Column('cart_id', db.Integer, db.ForeignKey('cart.id')))


class ProductType(enum.IntEnum):
    wok = 0
    burger = 1
    snack = 2
    cake = 3
    roll = 4
    drink = 5

    def default(self, o):
        return JSONEncoder.default(self, o)


class Product(PaginatedAPIMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), index=True, unique=True)
    description = db.Column(db.String(256))
    price = db.Column(db.Integer)
    type = db.Column(db.Enum(ProductType))
    weight = db.Column(db.Integer)
    # rate = db.Column(db.Float, default=5)
    components = db.relationship('Component', backref='product', lazy='dynamic')

    def __repr__(self):
        return f'Product {ProductType(self.type).name}:{self.title}'

    def to_dict(self):
        data = {
            'id': self.id,
            'title': self.title,
            'description': self.description,
            'price': self.price,
            'type': self.type,
            'weight': self.weight,
            'components': self.get_components()
        }
        return data

    def from_dict(self, data):
        for field in ['title', 'description', 'price', 'type', 'weight', 'components']:
            if field in data:
                setattr(self, field, data[field])

    def get_components(self):
        components = [
                component.to_dict() for component in self.components
            ]
        return components


class Component(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64), unique=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))

    def __repr__(self):
        return f'{self.title}'

    def to_dict(self):
        data = {
            'id': self.id,
            'title': self.title
        }
        return data


class Cart(db.Model):
    def get_total_price(self):
        products = Product.query.join(
            cart_product, (cart_product.c.cart_id == self.id))
        total_price = sum(product.price for product in products)
        return total_price

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    total_price = db.Column(db.Integer, default=get_total_price)


class OrderStatus(enum.Enum):
    created = 0
    paid = 1
    cooking = 2
    in_delivery = 3
    delivered = 4


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.Enum(OrderStatus))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    delivery_at = db.Column(db.DateTime)
    expectation_delivery_time = db.Column(db.DateTime, default=lambda x: x.created_at + timedelta(minutes=30))
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'))
    cart = db.relationship('Cart', backref='order')
    address = db.Column(db.String(64))
    payment_method = db.Column(db.String(64))
    comment = db.Column(db.String(256))


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return f'<User {self.username}>'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))