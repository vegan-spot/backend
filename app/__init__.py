from flask import Flask
from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_admin import Admin

from config import Config

db = SQLAlchemy()
migrate = Migrate()
login = LoginManager()
admin = Admin(name='fat-vegan-spot')


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)
    db.init_app(app)
    migrate.init_app(app, db)
    login.init_app(app)
    admin.init_app(app)

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from app.models import Product, Component
    admin.add_view(ModelView(Product, db.session))
    admin.add_view(ModelView(Component, db.session))

    return app


